import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";
import User from "./Pages/User/User.component"
import MultipleCheckbox from "./Pages/MultipleCheckbox/MultipleCheckbox.component"
import FormInsideForm from "./Pages/Test/FormInsideForm.component"
import RouteWithLayout from "./Components/RouteWithLayout/RouteWithLayout";
import Layout from "./Components/Layout/Layout.component"
import FindAppBar from "./Components/FindAppBar/FindAppBard.component"

function Routes() {
  return (
    <Switch>
      <Redirect exact from="/" to="/user" />
      <RouteWithLayout
        exact
        bar={FindAppBar}
        layout={Layout}
        path="/user"
        component={User}
        authenticate
      />
      <RouteWithLayout
        exact
        bar={FindAppBar}
        layout={Layout}
        path="/test"
        component={FormInsideForm}
        authenticate
      />
      <RouteWithLayout
        exact
        bar={FindAppBar}
        layout={Layout}
        path="/multicheckbox"
        component={MultipleCheckbox}
        authenticate
      />
    </Switch>
  )
}

export default Routes