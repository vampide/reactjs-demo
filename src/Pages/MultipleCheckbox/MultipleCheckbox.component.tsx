import React, { useEffect } from "react";
import { withStyles } from "@material-ui/core";
import { useForm } from "react-hook-form";
import { styles } from "./MultipleCheckbox.styles"

export interface MultipleCheckboxProps {

}

function MultipleCheckboxComponent(props: MultipleCheckboxProps) {
  const methods = useForm()

  useEffect(()=>{})

  const onSubmit = (data:any) => {
    console.log(data);
  }

  return (
    <>
      <form onSubmit={methods.handleSubmit(onSubmit)} >
        aa <input name="xx" type="checkbox" value="x1" ref={methods.register}></input>
        aa <input name="xx" type="checkbox" value="x2" ref={methods.register}></input>
        <div>
        aa <input name="xx" type="checkbox" value="x1" ref={methods.register}></input>
        <div>
        aa <input name="xx" type="checkbox" value="x1" ref={methods.register}></input>
        </div>
        </div>
        <button type="submit">xxx</button>
      </form>
    </>
  )
}

export default withStyles(styles)(MultipleCheckboxComponent)