import React, { ComponentType } from "react";
import { useForm } from "react-hook-form";
import { createStyles, Theme, WithStyles, withStyles } from "@material-ui/core";

const styles = (theme: Theme) =>
  createStyles({});

interface FormInsideFormProps extends WithStyles<typeof styles> {}

function FormInsideFormComponent(props: FormInsideFormProps) {
  const {
    classes,
  } = props;

  const methods1 = useForm();
  const methods2 = useForm();
  const handleSubmitForm1 = (data: any) => {
    console.log(data)
  }
  const handleSubmitForm2 = (data: any) => {
    console.log(data)
  }

  return (
    <>
      <form id="saveForm" onSubmit={methods1.handleSubmit(handleSubmitForm1)} >
        <div id="toolbar">
          <input type="text" name="x1" value="save1" ref={methods1.register} />
          <input type="text" name="x2" value="save2" ref={methods1.register} />
          <input type="text" name="x1" value="delete1" ref={methods2.register} />
          <input type="text" name="x2" value="delete2" ref={methods2.register} />
          <input type="submit" form="saveForm" />
          <input type="submit" form="deleteForm" />
        </div>
      </form>
      <form id="deleteForm" onSubmit={methods2.handleSubmit(handleSubmitForm2)} />
    </>
  );
}

export default withStyles(styles)(FormInsideFormComponent);
