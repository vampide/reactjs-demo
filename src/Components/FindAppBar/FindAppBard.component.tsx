import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles";
const lightColor = "rgba(255, 255, 255, 0.7)";
const styles = (theme: Theme) =>
  createStyles({
    secondaryBar: {
      padding: 5,
      zIndex: 0,
      backgroundColor: "#f2f4f8",
      height: 55,
    },
    menuButton: {
      marginLeft: -theme.spacing(1),
    },
    iconButtonAvatar: {
      padding: 4,
    },
    link: {
      textDecoration: "none",
      color: lightColor,
      "&:hover": {
        color: theme.palette.common.white,
      },
    },
    button: {
      borderColor: lightColor,
    },
    dividerBar: {
      backgroundColor: "#e0e1e3",
    },
    test:{
      marginTop:-30,
    }
  });

interface HeaderProps extends WithStyles<typeof styles> {}

function Header(props: HeaderProps) {
  const { classes } = props;

  return (
    <div className={classes.test}></div>
  );
}

export default withStyles(styles)(Header);
