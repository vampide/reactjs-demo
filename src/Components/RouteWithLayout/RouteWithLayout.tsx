import React, { ComponentType } from "react";
import { Redirect, Route } from "react-router-dom";
import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";
import { createStyles, Theme, WithStyles, withStyles } from "@material-ui/core";

const styles = (theme: Theme) =>
  createStyles({
    loading: {
      width: "100%",
      height: "100%",
      top: 0,
      left: 0,
      position: "fixed",
      display: "block",
      opacity: 0.5,
      backgroundColor: "#000",
      zIndex: 99999,
      textAlign: "center",
    },
    loadingCircular: {
      position: "absolute",
      zIndex: 100000,
      top: "50%",
      left: "calc(50% - 120px)",
    },
    textIndicator: {
      color: theme.palette.common.white,
    },
    circleProgress: {
      color: "#2957fd",
    },
  });

interface RouteWithLayoutPropTypes extends WithStyles<typeof styles> {
  component: any;
  layout: any;
  path: string;
  exact?: boolean;
  authenticate?: boolean;
  bar?: ComponentType;
}

function RouteWithLayout(props: RouteWithLayoutPropTypes) {
  const isAuthenticated = true;
  const loading = false;
  const {
    authenticate,
    layout: Layout,
    component: Component,
    bar,
    classes,
    ...rest
  } = props;

  return (
    <Route
      {...rest}
      render={(matchProps) => {
        if (authenticate) {
          return isAuthenticated ? (
            <Layout bar={bar}>
              {loading ? (
                <div className={classes.loading}>
                  <div className={classes.loadingCircular}>
                    <CircularProgress
                      thickness={5}
                      className={classes.circleProgress}
                      variant="indeterminate"
                    />
                    <Typography
                      variant="body2"
                      className={classes.textIndicator}
                    >
                      Đang xử lý, vui lòng chờ trong giây lát...
                    </Typography>
                  </div>
                </div>
              ) : null}
              <Component {...matchProps} />
            </Layout>
          ) : (
            <Redirect to="/login"/>
          );
        }
        return (
          <Layout>
            <Component {...matchProps}/>
          </Layout>
        );
      }}
    />
  );
}

export default withStyles(styles)(RouteWithLayout);
