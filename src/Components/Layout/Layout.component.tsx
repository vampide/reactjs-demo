import React, { ComponentType } from 'react';
import { createStyles, Theme, withStyles, WithStyles } from "@material-ui/core";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      minHeight: '100vh',
    },
    app: {
      flex: 1,
      display: 'flex',
      flexDirection: 'column',
    },
    main: {
      flex: 1,
      padding: theme.spacing(6, 4),
      background: '#f2f4f8',
    },
    footer: {
      padding: theme.spacing(2),
      background: '#f2f4f8',
    },
  });


interface LayoutProps extends WithStyles<typeof styles> {
  children: ComponentType,
  bar: ComponentType
}

function DashboardLayout(props: LayoutProps) {
  const { classes, children, bar: AppBar } = props;
  return (
    <div className={classes.root}>
      <div>đây là nav bar bên trái</div>
      <div className={classes.app}>
        <AppBar />
        <main className={classes.main}>
          {children}
        </main>
        <footer className={classes.footer}>
        </footer>
      </div>
    </div>
  );
}

export default withStyles(styles)(DashboardLayout);